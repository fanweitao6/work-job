package com.work.app.respone;

import java.util.Map;

public class ResponseData {
	
	private boolean succ = false;
	
	private String msg;
	
	private Map<String,Object> data ;

	public boolean isSucc() {
		return succ;
	}

	public void setSucc(boolean succ) {
		this.succ = succ;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
}
