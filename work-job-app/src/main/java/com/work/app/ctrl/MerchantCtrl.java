package com.work.app.ctrl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.work.app.respone.ResponseData;
import com.work.job.persist.entity.Merchant;
import com.work.job.service.MerchantService;
import com.work.job.util.CheckUtils;

@Controller
@RequestMapping("/merchant")
public class MerchantCtrl {
	
	@Autowired
	private MerchantService merchantService;
	
	@RequestMapping("/register")
	@ResponseBody
	public ResponseData merchantRegister(@RequestBody Merchant merchant){
		
		ResponseData data = new ResponseData();
		if(merchant == null){
			data.setMsg("注册信息不允许为空");
			return data;
		}
		if(CheckUtils.isNull(merchant.getPhone())){
			data.setMsg("手机号码不允许为空！");
			return data;
		}
		Merchant mer = merchantService.getMerchantByPhone(merchant.getPhone());
		if(mer != null){
			data.setMsg("该手机号已存在，请登录！");
			return data;
		}
		int row = merchantService.addMerchant(merchant);
		if(row != 1){
			data.setMsg("注册失败！");
			return data;
		}
		data.setSucc(true);
		data.setMsg("注册成功");
		return data;
	}
	@RequestMapping("/login")
	@ResponseBody
	public ResponseData merchentLogin(String phone,String pwd){
		
		ResponseData data = new ResponseData();
		if(CheckUtils.isNull(phone)){
			data.setMsg("手机号码不允许为空！");
			return data;
		}else if(CheckUtils.isNull(pwd)){
			data.setMsg( "密码不允许为空！");
			return data;
		}
		Merchant merchant = merchantService.getMerchantByPhonePwd(phone, pwd);
		if(merchant == null){
			data.setMsg("商户信息不存在，请注册！") ;
			return data;
		}
		data.setMsg("登录成功！");
		data.setSucc(true);
		Map<String,Object> map = new HashMap<>();
		map.put("data", merchant);
		data.setData(map);
		return data;
	}

	@RequestMapping("/countJob")
	public ModelAndView countJobInfoByMerchant(){
			String msg = null;
//			if(CheckUtils.isNull(username)){
//				msg = "手机号码不允许为空！";
//				
//			}else if(CheckUtils.isNull(password)){
//				msg = "密码不允许为空！";
//			}
//			Merchant merchant = merchantService.getMerchantByPhonePwd(username, password);
//			if(merchant == null){
//				msg = "商户信息不存在，请注册！" ;
//			}
			
//			data.setMsg("登录成功！");
//			data.setSucc(true);
//			Map<String,Object> map = new HashMap<>();
//			map.put("data", merchant);
//			data.setData(map);
			ModelAndView mv = new ModelAndView("/merchant/homePage");
			mv.addObject("msg",msg);
			return mv;

	}
}
