package com.work.app.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.work.job.persist.entity.Test;
import com.work.job.service.TestService;

@Controller
@RequestMapping("/test")
public class TestCtrl {
	
	@Autowired
	TestService testService;
	
	@RequestMapping(value = "/getTest" ,method = RequestMethod.GET)
	@ResponseBody
	public Test getTestById(int id){
		System.out.println("测试 接口");
		
		return testService.getTestById(id);
	}
	
	
}
