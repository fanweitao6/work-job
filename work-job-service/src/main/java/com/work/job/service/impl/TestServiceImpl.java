package com.work.job.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.work.job.persist.dao.TestDao;
import com.work.job.persist.entity.Test;
import com.work.job.service.TestService;

@Service("testService")
public class TestServiceImpl implements TestService {
	
	@Resource
	TestDao testDao;

	public Test getTestById(int id) {
		// TODO Auto-generated method stub
		return testDao.getTestById(id);
	}

}
