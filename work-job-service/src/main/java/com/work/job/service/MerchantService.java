package com.work.job.service;

import com.work.job.persist.entity.Merchant;

public interface MerchantService {
	
	int addMerchant(Merchant merchant);
	
	Merchant getMerchantByPhonePwd(String phone,String pwd);
	
	Merchant getMerchantByPhone(String phone);

}
