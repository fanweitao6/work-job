package com.work.job.service;

import com.work.job.persist.entity.Test;

public interface TestService {
	
	Test getTestById(int id);

}
