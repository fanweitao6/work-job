package com.work.job.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.work.job.persist.dao.MerchantDao;
import com.work.job.persist.entity.Merchant;
import com.work.job.service.MerchantService;

@Service("merchantService")
public class MerchantServiceImpl implements MerchantService {
	
	@Autowired
	private MerchantDao merchantDao;

	@Override
	public int addMerchant(Merchant merchant) {
		return merchantDao.insert(merchant);
	}

	@Override
	public Merchant getMerchantByPhonePwd(String phone, String pwd) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("phone", phone);
		map.put("pwd", pwd);
		return merchantDao.getMerchantByPhone(map);
	}

	@Override
	public Merchant getMerchantByPhone(String phone) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("phone", phone);
		return merchantDao.getMerchantByPhone(map);
	}

}
