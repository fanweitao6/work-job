package com.work.job.util;

public class CheckUtils {
	
	public static boolean isNull(String str){
		return str == null || str.length() == 0?true:false;
	}
	
	public static boolean isNotNull(String str){
		return !"".equals(str.trim()) || str != null ?true:false;
	}

}
