package test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.work.job.persist.dao.TestDao;
import com.work.job.persist.entity.Test;

public class MyBatisTest {

	public static void main(String[] args) {
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:config//spring//datasource.xml");
		TestDao td = (TestDao) ac.getBean("testDao");
		Test t = td.getTestById(31);
		System.out.println(t.getName());
	}
}
