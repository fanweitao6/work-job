package com.work.job.persist.dao;

import java.util.Map;

import com.work.job.persist.entity.Merchant;

public interface MerchantDao {
    int deleteByPrimaryKey(Long id);

    int insert(Merchant record);

    int insertSelective(Merchant record);

    Merchant selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Merchant record);

    int updateByPrimaryKey(Merchant record);
    
    Merchant getMerchantByPhone(Map<String,Object> map);
}