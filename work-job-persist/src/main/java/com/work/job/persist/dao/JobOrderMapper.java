package com.work.job.persist.dao;

import com.work.job.persist.entity.JobOrder;

public interface JobOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(JobOrder record);

    int insertSelective(JobOrder record);

    JobOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JobOrder record);

    int updateByPrimaryKey(JobOrder record);
}