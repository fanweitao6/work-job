package com.work.job.persist.dao;

import com.work.job.persist.entity.JobType;

public interface JobTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(JobType record);

    int insertSelective(JobType record);

    JobType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JobType record);

    int updateByPrimaryKey(JobType record);
}