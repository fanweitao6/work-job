package com.work.job.persist.dao;

import com.work.job.persist.entity.JobTypeDetail;

public interface JobTypeDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(JobTypeDetail record);

    int insertSelective(JobTypeDetail record);

    JobTypeDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JobTypeDetail record);

    int updateByPrimaryKey(JobTypeDetail record);
}