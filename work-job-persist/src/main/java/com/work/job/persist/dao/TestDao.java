package com.work.job.persist.dao;

import java.util.List;

import com.work.job.persist.entity.Test;

public interface TestDao {
	
	List<Test> getAllList();
	
	int add(Test t);
	
	int update(Test t);
	
	Test getTestById(int id);
	
	int delById(int id);

}
