package com.work.job.persist.entity;

public class JobInfo {
    private Long id;

    private String jobId;

    private String addressId;

    private String jobName;

    private String jobType;

    private String salary;

    private String settlementCycle;

    private String sexRequire;

    private String ageRequire;

    private String academicRequire;

    private String jobLabel;

    private String recruitsTime;

    private String jobTime;

    private String jobContent;

    private String linkman;

    private String phone;

    private String consultType;

    private Integer recruitsNum;

    private String status;

    private String resv1;

    private String resv2;

    private String createTime;

    private String updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId == null ? null : jobId.trim();
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId == null ? null : addressId.trim();
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName == null ? null : jobName.trim();
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType == null ? null : jobType.trim();
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary == null ? null : salary.trim();
    }

    public String getSettlementCycle() {
        return settlementCycle;
    }

    public void setSettlementCycle(String settlementCycle) {
        this.settlementCycle = settlementCycle == null ? null : settlementCycle.trim();
    }

    public String getSexRequire() {
        return sexRequire;
    }

    public void setSexRequire(String sexRequire) {
        this.sexRequire = sexRequire == null ? null : sexRequire.trim();
    }

    public String getAgeRequire() {
        return ageRequire;
    }

    public void setAgeRequire(String ageRequire) {
        this.ageRequire = ageRequire == null ? null : ageRequire.trim();
    }

    public String getAcademicRequire() {
        return academicRequire;
    }

    public void setAcademicRequire(String academicRequire) {
        this.academicRequire = academicRequire == null ? null : academicRequire.trim();
    }

    public String getJobLabel() {
        return jobLabel;
    }

    public void setJobLabel(String jobLabel) {
        this.jobLabel = jobLabel == null ? null : jobLabel.trim();
    }

    public String getRecruitsTime() {
        return recruitsTime;
    }

    public void setRecruitsTime(String recruitsTime) {
        this.recruitsTime = recruitsTime == null ? null : recruitsTime.trim();
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime == null ? null : jobTime.trim();
    }

    public String getJobContent() {
        return jobContent;
    }

    public void setJobContent(String jobContent) {
        this.jobContent = jobContent == null ? null : jobContent.trim();
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getConsultType() {
        return consultType;
    }

    public void setConsultType(String consultType) {
        this.consultType = consultType == null ? null : consultType.trim();
    }

    public Integer getRecruitsNum() {
        return recruitsNum;
    }

    public void setRecruitsNum(Integer recruitsNum) {
        this.recruitsNum = recruitsNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getResv1() {
        return resv1;
    }

    public void setResv1(String resv1) {
        this.resv1 = resv1 == null ? null : resv1.trim();
    }

    public String getResv2() {
        return resv2;
    }

    public void setResv2(String resv2) {
        this.resv2 = resv2 == null ? null : resv2.trim();
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}