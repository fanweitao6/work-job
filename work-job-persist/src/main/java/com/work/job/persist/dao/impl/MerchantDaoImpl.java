package com.work.job.persist.dao.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.work.job.persist.dao.MerchantDao;
import com.work.job.persist.entity.Merchant;

@Repository("merchantDao")
public class MerchantDaoImpl extends BaseDao implements MerchantDao {

	@Override
	public int deleteByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Merchant record) {
		return sqlSession.insert("merchant.insert", record);
	}

	@Override
	public int insertSelective(Merchant record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Merchant selectByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateByPrimaryKeySelective(Merchant record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKey(Merchant record) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Merchant getMerchantByPhone(Map<String, Object> map) {
		return sqlSession.selectOne("merchant.getMerchantByPhone", map);
	}

}
