package com.work.job.persist.dao;

import com.work.job.persist.entity.ConsultInfo;

public interface ConsultInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ConsultInfo record);

    int insertSelective(ConsultInfo record);

    ConsultInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ConsultInfo record);

    int updateByPrimaryKey(ConsultInfo record);
}