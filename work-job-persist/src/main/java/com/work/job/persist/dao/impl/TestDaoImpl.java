package com.work.job.persist.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.work.job.persist.dao.TestDao;
import com.work.job.persist.entity.Test;

@Repository("testDao")
public class TestDaoImpl extends BaseDao implements TestDao {
	
	public List<Test> getAllList() {
		// TODO Auto-generated method stub
		return null;
	}

	public int add(Test t) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int update(Test t) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Test getTestById(int id) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("test.getTestById",id);
	}

	public int delById(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
