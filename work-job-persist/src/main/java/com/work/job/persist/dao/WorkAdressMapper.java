package com.work.job.persist.dao;

import com.work.job.persist.entity.WorkAdress;

public interface WorkAdressMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WorkAdress record);

    int insertSelective(WorkAdress record);

    WorkAdress selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WorkAdress record);

    int updateByPrimaryKey(WorkAdress record);
}