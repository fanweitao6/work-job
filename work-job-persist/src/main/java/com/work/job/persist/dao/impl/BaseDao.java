package com.work.job.persist.dao.impl;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseDao{

	@Autowired
	public SqlSessionTemplate sqlSession;
	
	public void setSqlSession(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSession = sqlSessionTemplate;
	}
}
