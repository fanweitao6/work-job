package com.work.job.persist.dao;

import com.work.job.persist.entity.JobDetail;

public interface JobDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(JobDetail record);

    int insertSelective(JobDetail record);

    JobDetail selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JobDetail record);

    int updateByPrimaryKey(JobDetail record);
}